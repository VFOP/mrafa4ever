package rafanadal.rafa4ever;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FLORINOPREA93 on 5/17/2016.
 */
public class Matches {
    private List<Match> matchList = new ArrayList<>();

    public Matches() {
    }

    public List<Match> getMatchList() {
        return matchList;
    }

    public void setMatchList(List<Match> matchList) {
        this.matchList = matchList;
    }

    private static class MatchesHolder {
        private static final Matches INSTANCE = new Matches();
    }

    public static Matches getInstance() {
        return MatchesHolder.INSTANCE;
    }
}
