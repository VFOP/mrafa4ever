package rafanadal.rafa4ever;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AddMatches extends AppCompatActivity {

    List<Match> matchList = new ArrayList<>();

    TextView opponentName;
    TextView tournamentName;
    TextView tournamentType;
    TextView tournamentSurface;
    TextView matchScore;
    TextView matchRound;
    TextView matchResult;
    TextView year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_matches);

        setupVariables();
        setupUIEvents();

    }

    public void setupVariables() {
        opponentName = (TextView) findViewById(R.id.opponentName);
        tournamentName = (TextView) findViewById(R.id.tournamentName);
        tournamentType = (TextView) findViewById(R.id.tournamentType);
        tournamentSurface = (TextView) findViewById(R.id.tournamentSurface);
        matchScore = (TextView) findViewById(R.id.matchScore);
        matchRound = (TextView) findViewById(R.id.matchRound);
        matchResult = (TextView) findViewById(R.id.matchResult);
        year = (TextView) findViewById(R.id.year);
    }

    public void setupUIEvents() {
        Button newBookOkButton = (Button) findViewById(R.id.saveButton);
        newBookOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSaveButtonOnClick((Button) v);
            }
        });
    }

    public void handleSaveButtonOnClick(Button newMatchButton) {
        String sOpponentName = opponentName.getText().toString();
        String sTournamentName = tournamentName.getText().toString();
        String sTournamentType = tournamentType.getText().toString();
        String sTournamentSurface = tournamentSurface.getText().toString();
        String sMatchScore = matchScore.getText().toString();
        String sMatchRound = matchRound.getText().toString();
        String sMatchResult = matchResult.getText().toString();
        String sYear = year.getText().toString();

        Match match = new Match(sOpponentName, sTournamentName, sTournamentType, sTournamentSurface, sMatchScore, sMatchRound, sMatchResult, sYear);
        matchList.add(match);

        Matches.getInstance().setMatchList(matchList);

        startActivity(new Intent(getApplicationContext(), AdminActivity.class));
    }
}
