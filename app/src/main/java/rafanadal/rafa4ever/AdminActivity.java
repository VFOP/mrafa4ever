package rafanadal.rafa4ever;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import rafanadal.rafa4ever.ATP250;
import rafanadal.rafa4ever.ATP500;
import rafanadal.rafa4ever.GrandSlams;
import rafanadal.rafa4ever.Masters1000;
import rafanadal.rafa4ever.R;

public class AdminActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        setupUIEvents();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_admin, menu);
        return true;
    }

    public  boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        boolean handled = true;

        switch (id) {
            case R.id.action_showGrandSlams:
                onClickMenuShowGrandSlams(item);
                break;
            case R.id.action_showMasters1000:
                onClickMenuShowMasters1000(item);
                break;
            case R.id.action_showATP500:
                onClickMenuShowATP500(item);
                break;
            case R.id.action_showATP250:
                onClickMenuShowATP250(item);
                break;
            case R.id.action_exit:
                onClickMenuExit(item);
                break;
            default:
                handled = super.onOptionsItemSelected(item);
        }

        return handled;

    }

    public void onClickMenuShowGrandSlams(MenuItem item) {
        Intent intent = new Intent(this, GrandSlams.class);
        startActivity(intent);
    }

    public void onClickMenuShowMasters1000(MenuItem item) {
        Intent intent = new Intent(this, Masters1000.class);
        startActivity(intent);
    }

    public void onClickMenuShowATP500(MenuItem item) {
        Intent intent = new Intent(this, ATP500.class);
        startActivity(intent);
    }

    public void onClickMenuShowATP250(MenuItem item) {
        Intent intent = new Intent(this, ATP250.class);
        startActivity(intent);
    }

    public void onClickMenuExit(MenuItem item) {
        finish();
    }

    public void setupUIEvents() {

        Button addMatchButton = (Button) findViewById(R.id.button2);
        if (addMatchButton != null) {
            addMatchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("Am intrat in metoda!!");
                    handleLoginButtonClick((Button) v);
                }
            });
        }
    }

    void handleLoginButtonClick(Button loginButton) {
        //Intent intent = new Intent(this, Masters1000.class);
        startActivity(new Intent(getApplicationContext(), AddMatches.class));
    }

}
