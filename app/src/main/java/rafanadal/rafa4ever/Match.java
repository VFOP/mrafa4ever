package rafanadal.rafa4ever;

/**
 * Created by FLORINOPREA93 on 5/17/2016.
 */
public class Match {

    private String opponentName;
    private String tournamentName;
    private String tournamentType;
    private String tournamentSurface;
    private String matchScore;
    private String matchRound;
    private String matchResult;
    private String year;

    public Match(String opponentName, String tournamentName, String tournamentType, String tournamentSurface, String matchScore, String matchRound, String matchResult, String year) {
        this.opponentName = opponentName;
        this.tournamentName = tournamentName;
        this.tournamentType = tournamentType;
        this.tournamentSurface = tournamentSurface;
        this.matchScore = matchScore;
        this.matchRound = matchRound;
        this.matchResult = matchResult;
        this.year = year;
    }

    public String getOpponentName() {
        return opponentName;
    }

    public void setOpponentName(String opponentName) {
        this.opponentName = opponentName;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public String getTournamentType() {
        return tournamentType;
    }

    public void setTournamentType(String tournamentType) {
        this.tournamentType = tournamentType;
    }

    public String getTournamentSurface() {
        return tournamentSurface;
    }

    public void setTournamentSurface(String tournamentSurface) {
        this.tournamentSurface = tournamentSurface;
    }

    public String getMatchScore() {
        return matchScore;
    }

    public void setMatchScore(String matchScore) {
        this.matchScore = matchScore;
    }

    public String getMatchRound() {
        return matchRound;
    }

    public void setMatchRound(String matchRound) {
        this.matchRound = matchRound;
    }

    public String getMatchResult() {
        return matchResult;
    }

    public void setMatchResult(String matchResult) {
        this.matchResult = matchResult;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
